<?php

namespace Phr\Eojwt;


interface IEndpoints 
{
    public const INFO = 'auth/info';

    public const USER = 'auth/user';

    public const TOKEN = 'auth/token';

    public const ADMIN = 'auth/admin';

}
