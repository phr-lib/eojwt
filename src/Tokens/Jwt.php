<?php

namespace Phr\Eojwt\Tokens;

use Phr\Eojwt\JwtModel\EoJwtTokenModel;
use Phr\Eojwt\EoJwtBase\Endpoints;
use Phr\Eojwt\EoJwtBase\TokenType;


class Jwt extends EoJwtTokenModel
{   
    public object $content;

    public function populate(array $_data, object $_content){  
        $this->setData($_data);
        $this->content = $_content;
    }

    public function content(string $_jwt_content, TokenType $type): self
    {
        $content = $this->object($_jwt_content);
        
        $this->setData([
            $content->issuer,
            $content->realmId,
            $content->clientId
        ]);
        $this->setTimeHash($content->timehash);
        $this->version = $content->version;
        $this->content = $content->content;

        $this->contentHash =  $this->hash($type->algoContent());
        return $this;
    }
}