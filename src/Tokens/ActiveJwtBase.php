<?php

namespace Phr\Eojwt\Tokens;

abstract class ActiveJwtBase 
{
    public string $serverId;

    public string $clientIp;

    public string $sessionId;

    public string $userIdHash;

    public string $expire;
}