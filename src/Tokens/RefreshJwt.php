<?php

namespace Phr\Eojwt\Tokens;

use Phr\Eojwt\JwtModel\EoJwtTokenModel;
use Phr\Eojwt\Accounts\ActiveSession;
use Phr\Eojwt\EoJwtBase\TokenType;
use Phr\Eojwt\Accounts\SessionFp;


class RefreshJwt extends EoJwtTokenModel
{   
    public ActiveSession $session;


    public function add(ActiveSession $_session)
    {  
        $this->session = $_session;
    }

    public function content(string $_jwt_content, TokenType $type): self
    {   
        $this->contentHash = $_jwt_content;
        $content = $this->object($_jwt_content);
        $this->setData([
            $content->issuer,
            $content->realmId,
            $content->clientId
        ]);
        
        $this->session = new ActiveSession(
            $content->session->accountId,
            new SessionFp(
                [
                    $content->session->sessionFp->fp1,
                    $content->session->sessionFp->fp2,
                    $content->session->sessionFp->fp3,
                    $content->session->sessionFp->fp4,
                    $content->session->sessionFp->fp5,

                ]
            )
            
        );
        
        return $this;
    }
}