<?php

namespace Phr\Eojwt\Tokens;

use Phr\Eojwt\JwtModel\SecureTokenModel;
use Phr\Eojwt\Accounts\SessionAccount;
use Phr\Eojwt\Accounts\ServerAccount;
use Phr\Eojwt\Accounts\SessionFp;

/**
 * 
 * Server account token. 
 * 
 */
class ServJwt extends SecureTokenModel
{   
    public ServerAccount $account;

    public function add(ServerAccount $_account)
    {
        $this->account = $_account;
    }
    public function content(string $_encoded_content): self
    {   
        
        $content =  $this->serverDecrypt($_encoded_content);
        
        $this->timehash = $content->timehash;
        $account = $content->account;
        $this->account = new ServerAccount(
            $account->serverId,
            new SessionAccount(
                $account->sessionAccount->userId,
                $account->sessionAccount->sessionId,
                $account->sessionAccount->sessionTs,
                $account->sessionAccount->expire,
                $account->sessionAccount->enryptor,
                $account->sessionAccount->sessionIv,
                new SessionFp([
                    $account->sessionAccount->sessionFingerprints->fp1,
                    $account->sessionAccount->sessionFingerprints->fp2,
                    $account->sessionAccount->sessionFingerprints->fp3,
                    $account->sessionAccount->sessionFingerprints->fp4,
                    $account->sessionAccount->sessionFingerprints->fp5,
                ])
            ),
            $account->clientIp
        );
        return $this;
    }
}