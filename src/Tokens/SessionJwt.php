<?php

namespace Phr\Eojwt\Tokens;

use Phr\Eojwt\Accounts\SessionAccount;
use Phr\Eojwt\Accounts\SessionFp;
use Phr\Eojwt\JwtModel\SecureTokenModel;


class SessionJwt extends SecureTokenModel
{   
    public SessionAccount $account;

    public function add(SessionAccount $_account)
    {
        $this->account = $_account;
    }
    public function content(string $_encoded_content): self
    {   
        $content =  $this->decryptContent($_encoded_content);
        $this->timehash = $content->timehash;
        $this->account = new SessionAccount(
            $content->account->userId,
            $content->account->sessionId,
            $content->account->sessionTs,
            $content->account->expire,
            $content->account->sessionIv,
            $content->account->enryptor,
            new SessionFp(
                [
                    $content->account->sessionFingerprints->fp1,
                    $content->account->sessionFingerprints->fp2,
                    $content->account->sessionFingerprints->fp3,
                    $content->account->sessionFingerprints->fp4,
                    $content->account->sessionFingerprints->fp5
                ]

            )
        );
        return $this;
    }
}