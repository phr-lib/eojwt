<?php

namespace Phr\Eojwt\Tokens;

use Phr\Eojwt\Accounts\UserAccount;
use Phr\Eojwt\JwtModel\SecureTokenModel;
use Phr\Eojwt\Accounts\UserRols;

/**
 * 
 * User account jwt.
 * 
 * 
 */
class SecureAccountJwt extends SecureTokenModel
{   
    public UserAccount $account;

    public function add(UserAccount $_account)
    {
        $this->account = $_account;
    }
    public function content(string $_content)
    {      
        $this->setEcodedPayload($_content);
    }
    public function decryptPayload(string $_key, string $_session_iv): void
    {      
        $d = $this->decrypt($_key, $_session_iv);
        $content = json_decode($d);
        $userRols = [];
        
        $this->account = new UserAccount(
            $content->account->username,
            $content->account->accountId,
            $content->account->sessionId,
            $content->account->enryptor,
            $content->account->sessionIv,
            new UserRols($userRols)
        );
    }
      
}