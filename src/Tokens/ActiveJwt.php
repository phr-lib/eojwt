<?php

namespace Phr\Eojwt\Tokens;

use Phr\Eojwt\Accounts\ServerAccount;
use Phr\Certificator\Encryption;
use Phr\Certificator\Encry\HashAlgo as ALGO;
use Phr\Eojwt\Accounts\SessionFp;

class ActiveJwt extends ActiveJwtBase
{   
    public SessionFp $sessionFph;

    public function create(ServerAccount $_server_account)
    {   
        $this->serverId  = $S = $_server_account->serverId;
        
        $this->clientIp = $t = $_server_account->clientIp;
        
        $sessionAccount = $_server_account->sessionAccount;
        $this->sessionId = $al =  $sessionAccount->sessionId;
        
        $userId = $sessionAccount->userId;
        $h1 = Encryption::hashIt($sessionAccount->userId.$S.$al.$t, ALGO::SHA256);
        $this->userIdHash = $userId;

        $this->expire = (string)$sessionAccount->expire;

        $sessionFp = $sessionAccount->sessionFingerprints;
        $this->sessionFph = new SessionFp([
        $h1 = Encryption::hashIt($sessionFp->fp1.$S.$al.$t, ALGO::SHA256),
        $h2 = Encryption::hashIt($sessionFp->fp2.$S.$al.$t, ALGO::SHA256),
        $h3 = Encryption::hashIt($sessionFp->fp3.$S.$al.$t, ALGO::SHA256),
        $h4 = Encryption::hashIt($sessionFp->fp4.$S.$al.$t, ALGO::SHA256),
        $h5 = Encryption::hashIt($sessionFp->fp5.$S.$al.$t, ALGO::SHA256)
        ]);
    }
    public function encode(): string
    {
        return Encryption::baseEncode(json_encode($this));
    }
}