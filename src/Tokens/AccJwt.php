<?php

namespace Phr\Eojwt\Tokens;

use Phr\Eojwt\Accounts\UserAccount;
use Phr\Eojwt\JwtModel\SecureTokenModel;

/**
 * 
 * User account jwt.
 * 
 * 
 */
class AccJwt extends SecureTokenModel
{   
    public UserAccount $account;

    public function add(UserAccount $_account)
    {
        $this->account = $_account;
    }
    public function content(string $_content_encryptor): self
    {       
        $content =  $this->decryptContent($_content_encryptor);
        $this->account = new UserAccount(
            $content->account->username,
            $content->account->accountId,
            $content->account->userRols
        );
        $this->validateExpire($content->timehash);
        return $this;
    }   
}