<?php

namespace Phr\Eojwt\JwtModel\Signatures;

use Phr\Eojwt\EoJwtBase\SignatureModel;
use Phr\Eojwt\EoJwtBase\TokenType;


class EoSecureSignature extends SignatureModel
{   
    public function content(string $_encoded_signature): void
    {   
        $this->signature = $_encoded_signature;
    }
    public function validate( string $_key ): true
    {
        $this->contentHash = $this->decrypt($_key);
        return true;
    }
    public function getContentEncoder(): string
    {
        return $this->contentHash;
    }
    public function decrypt( string $_key ): string 
    {   
        return match (self::$TYPE) {
            TokenType::ACCJWT => $this->publicDecrypt($_key, $this->signature),
            TokenType::SERJWT => $this->privateDecrypt($_key, $this->signature),
        };
    }
}