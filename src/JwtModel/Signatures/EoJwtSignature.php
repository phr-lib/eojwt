<?php

namespace Phr\Eojwt\JwtModel\Signatures;

use Phr\Eojwt\EoJwtBase\SignatureModel;
use Phr\Eojwt\EoJwtBase\TokenType;
use Phr\Eojwt\JwtModel\Signatures\EoJwtSignatureVertification;
use Phr\Certificator\Encryption;
use Phr\Eojwt\EoJwtException;
use Phr\Certificator\Encry\HashAlgo as ALG;



/**
 * 
 * Used for PHRJWT
 * 
 * Part of the pHrJwt token.
 * 
 */
class EoJwtSignature extends SignatureModel
{
    public EoJwtSignatureVertification $verificationHashes;

    public function content(string $_encoded_signature): void
    {   
        /**
         * Decode signature into object.
         */
        $decoded = $this->object($_encoded_signature);
        /**
         * Main signature is enrypted in hex. Hex decode
         * and load into signature for 
         * validation.
         */
        $this->signature = $this->dehex($decoded->signature);

        if(!$decoded->verificationHashes->contentHash)
                        throw new EoJwtException(ERR::E5393201);

        if(!$decoded->verificationHashes->timeHash)
                        throw new EoJwtException(ERR::E5393201);
        /**
         * Cotanins content and time hash.
         * It is validated with verification 
         * hash. Verification has is encrypted
         * with private rsa key.
         * 
         */
        $this->verificationHashes = new EoJwtSignatureVertification(
            $decoded->verificationHashes->contentHash
            ,$decoded->verificationHashes->timeHash
        );
    }
   
    
    public function validate( string $_key ): true
    {   
        /**
         * Switch between token types.
         */
        switch ( self::$TYPE ) 
        {
            case TokenType::PHRJWT: 
                $signatureVerification = $this->publicDecrypt($_key, $this->signature); 
            break;
            case TokenType::PPPJWT: 
                $signatureVerification = $this->privateDecrypt($_key, $this->signature); 
            break;
        }
        /**
         * 
         * Verify signature
         * 
         */
        if($this->verificationHashes->hash(self::$TYPE->algoSignature()) == $signatureVerification)
        {   
            if($this->verificationHashes->timeHash == $this->timehash)return true;
            else throw new EoJwtException(ERR::E5393505);
        } 
        else throw new EoJwtException(ERR::E5393505);
    }

   
   
    
}