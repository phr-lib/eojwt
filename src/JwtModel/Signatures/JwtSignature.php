<?php

namespace Phr\Eojwt\JwtModel\Signatures;

use Phr\Eojwt\EoJwtBase\SignatureModel;
use Phr\Eojwt\EoJwtBase\TokenType;
use Phr\Eojwt\EoJwtException;
use Phr\Eojwt\EoJwtBase\Errors as ERR;

/**
 * 
 * Basic jwt signature. 
 * 
 * Used for PHRJWT and PPPJWT tokens. 
 * 
 * Decrypt signature based on token type
 * and matches it with token content 
 * hash.
 * 
 * 
 */
class JwtSignature extends SignatureModel
{   
    /**
     * 
     * @access public
     * @method stores encoded signature
     * 
     */
    public function content(string $_encoded_signature): void
    {
        $this->signature = $_encoded_signature;
    }
    /**
     * 
     * @method validates decypted signature
     * with content hash
     * @param string rsa key
     * @return true if it is valid,
     * @throws EoJwtException
     * 
     */
    public function validate( string $_key ): true
    {   
        /**
         * Decrypt key with rsa.
         */
        $this->signature = $this->decrypt($_key);
        /**
         * Match hashes !
         */
        if($this->signature === $this->contentHash)
        return true; else throw new EoJwtException(ERR::E5393502);   
    }
    /**
     * 
     * 
     * @access private
     * @method determinates decryption based
     * on token type
     * @param string rsa key
     * @return string decrpted signature
     * 
     * 
     */
    private function decrypt( string $_key )
    {
        return match (parent::$TYPE) {
            TokenType::PHRJWT => $this->publicDecrypt($_key, $this->signature),
            TokenType::PPPJWT => $this->privateDecrypt($_key, $this->signature),
            TokenType::SECJWT => $this->publicDecrypt($_key, $this->signature)

        };
    }
}