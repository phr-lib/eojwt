<?php

namespace Phr\Eojwt\JwtModel\Signatures\SubSig;

use Phr\Eojwt\EoJwtTools\EoJwtEncoder;

class RefreshSubSignature extends EoJwtEncoder
{
    public string $sessionFilePath;

    public string $timeHash;

    public function __construct(
        string $_session_file_path
        ,string $_time_hash
    ){
        $this->sessionFilePath = $_session_file_path;
        $this->timeHash = $_time_hash;
    }
}