<?php

namespace Phr\Eojwt\JwtModel\Signatures;

use Phr\Eojwt\EoJwtException;
use Phr\Eojwt\EoJwtBase\Errors as ERR;
use Phr\Eojwt\EoJwtBase\SignatureModel;

class SimpleTimeSignature extends SignatureModel 
{   

    public function content(string $_encoded_signature): void 
    {
        $this->signature = $_encoded_signature;
    }
    public function validate(string $_key): true 
    {
       
        $verify = self::simpleTimeSig($this->timehash, $_key);

        if( $verify !== $this->signature )
            throw new EoJwtException(ERR::E5393505);
       
        return true; 
    }
    public function setTimeHash(string $_time_hash)
    {
        $this->timehash = $_time_hash;
    }
}