<?php

namespace Phr\Eojwt\JwtModel\Signatures;

use Phr\Eojwt\EoJwtTools\EoJwtEncoder;

class EoJwtSignatureVertification extends EoJwtEncoder
{
    public string $contentHash;

    public string $timeHash;

    public function __construct(
        string $_content_hash
        ,string $_time_hash
    ){
        $this->contentHash = $_content_hash;
        $this->timeHash = $_time_hash;
    }
}