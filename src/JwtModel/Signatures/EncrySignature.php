<?php

namespace Phr\Eojwt\JwtModel\Signatures;

use Phr\Eojwt\EoJwtBase\SignatureModel;
use Phr\Eojwt\EoJwtBase\TokenType;

/**
 * 
 * Encryption signature. Used for
 * refresh token. 
 * 
 */
class EncrySignature extends SignatureModel
{   
    public string $encrySignature;
    
    public function content(string $_encoded_signature): void
    {   
        $sigParts = $this->object($_encoded_signature);      
        $this->signature = $this->dehex($sigParts->signature);
        $this->encrySignature = $this->dehex($sigParts->encrySignature);
    }
    public function validate( string $_key ): true
    {   
        /**
         * Decypt signature encoder.
         */
        $sigEncoder = $this->privateDecrypt($_key, $this->signature);
        /**
         * Decode encry signature.
         */
        $result = $this->sslDecrypt($sigEncoder, $this->encrySignature);
        if(!$result) throw new EoJwtException(ERR::E5393001);
        /**
         * Verify timehash.
         */
        if($this->jsonDecode($result)->timeHash !== $this->timehash)
                throw new EoJwtException(ERR::E5393001);
        /**
         * Save session file path
         */
        $this->signature = $this->jsonDecode($result)->sessionFilePath;
        return true;
    }
}