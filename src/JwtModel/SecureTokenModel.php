<?php

namespace Phr\Eojwt\JwtModel;

use Phr\Eojwt\EoJwtBase\EoJwtBase;
use Phr\Eojwt\EoJwtTools\EoJwtEncoder;
use Phr\Certificator\Encryption;


abstract class SecureTokenModel extends EoJwtEncoder
{
    public string $timehash;

    protected string $ecodedpayload;

    public function setEcodedPayload(string $_encoded_payload): void 
    {
        $this->ecodedpayload = $_encoded_payload;
    }
    public function encrypted(): string 
    {
        return $this->ecodedpayload;
    }
    public function decryptContent( string $_content_encryptor ): object
    {   
        $encrypt = new Encryption($_content_encryptor);
        $result = $encrypt->fernetDecrypt($this->ecodedpayload);
        if(!$result) throw new EoJwtException(ERR::E5393001);

        return $this->jsonDecode($result);  
    }
    public function setTimeHash(string $_time_hash)
    {
        $this->timehash = $_time_hash;
    }
    public function validateExpire( string $_jwt_timehash )
    {
        if($_jwt_timehash === $this->timehash) return true;
        else throw new EoJwtException(ERR::E5393001, "timehash");
    }
    public function decrypt( string $_key, string $_iv )
    {   
        return Encryption::decryptAES($this->ecodedpayload, $_key, $_iv);

    }
    public function serverDecrypt( string $_key )
    {
        $encryption = new Encryption($_key);
        $decrypt = $encryption->sslDecrypt($this->ecodedpayload);
        return json_decode($decrypt);
    }
}
