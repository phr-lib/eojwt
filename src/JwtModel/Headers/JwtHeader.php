<?php

namespace Phr\Eojwt\JwtModel\Headers;

use Phr\Eojwt\EoJwtTools\EoJwtEncoder;
use Phr\Eojwt\EoJwtBase\TokenType;

/**
 * 
 * Jwt header 
 * 
 * 
 * 
 */
class JwtHeader extends EoJwtEncoder
{   
    /**
     * @access public
     * @var string
     */
    public string $tokenId;
    
    public string $type;

    public \DateTime $issued;

    public string $sessionId;

    public string $expire;
    /**
     * @access private
     * @static
     * @var TokenType
     */
    private static TokenType $TYPE;

    /// CONSTRUCT ***
    public function content(string $_encrypted_header): void
    {
        $decoded = $this->object($_encrypted_header);
        
        $this->type = $decoded->type;
        
        $this->setType();
        $this->issued = new \DateTime($decoded->issued->date);
        $this->expire = $decoded->expire;
        $this->tokenId = $decoded->tokenId;
        if(isset($decoded->sessionId)) $this->sessionId = $decoded->sessionId;
    }
    /**
     * 
     * @access public
     * @method sets token type form
     * signature header.
     * @param TokenType type
     */
    public function type(TokenType $TYPE): void
    {   
        $this->type = $TYPE->type();
    }
    public function getType(): TokenType
    {
        return self::$TYPE;
    }
    /**
     * @access private
     */
    private function setType(): void
    {   
        match ($this->type) 
        {
            TokenType::PHRJWT->type() => self::$TYPE = TokenType::PHRJWT,
            TokenType::PPPJWT->type() => self::$TYPE = TokenType::PPPJWT,
            TokenType::ACCJWT->type() => self::$TYPE = TokenType::ACCJWT,
            TokenType::REFJWT->type() => self::$TYPE = TokenType::REFJWT,
            TokenType::SERJWT->type() => self::$TYPE = TokenType::SERJWT,
            TokenType::SECJWT->type() => self::$TYPE = TokenType::SECJWT,
            TokenType::ACTJWT->type() => self::$TYPE = TokenType::ACTJWT
        };
    }

}