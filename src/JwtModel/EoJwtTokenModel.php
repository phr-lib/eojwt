<?php

namespace Phr\Eojwt\JwtModel;

use Phr\Eojwt\IEoJwt;
use Phr\Eojwt\EoJwtBase\EoJwtBase;
use Phr\Eojwt\EoJwtTools\EoJwtEncoder;

/**
 * @abstract 
 * 
 * Token model
 * 
 */
abstract class EoJwtTokenModel extends EoJwtEncoder
{
    public string $issuer;

    public string $version = IEoJwt::VERSION;

    public string $realmId;

    public string $clientId;

    public string $timehash;


    /**
     * @access public
     * @method sets default 
     * token data
     * @param array data
     */
    public function setData(array $_data)
    {   
        $this->issuer = $_data[0];
        $this->realmId = $_data[1];
        $this->clientId = $_data[2];
    }
    public function setTimeHash(string $_time_hash)
    {
        $this->timehash = $_time_hash;
    }
    /**
     * @access protected
     * @param string content hash
     */
    protected string $contentHash;

    public function getContentHash(): string
    {
        return $this->contentHash;
    }
}
