<?php

namespace Phr\Eojwt;

use Phr\Eojwt\Accounts\UserAccount;
use Phr\Eojwt\Accounts\ServerAccount;
use Phr\Eojwt\Tokens\Jwt;
use Phr\Eojwt\Tokens\AccJwt;
use Phr\Eojwt\Tokens\RefreshJwt;
use Phr\Eojwt\Tokens\SessionJwt;
use Phr\Eojwt\Tokens\ServJwt;
use Phr\Eojwt\Accounts\ActiveToken;

/**
 * 
 * 
 * 
 * PHP version 8.2 or above
 * 
 * @category Authentication
 * @author Grega Lipovšček
 * @license https://lab.ortus.si
 * @see final class EoJwt
 * 
 * @link https://ortus.si
 * 
*/
interface IEoJwt 
{   
    public const VERSION = '1.0.1';
    
    public const PHRJWT = 'PHRJWT';

    public const PPPJWT = 'PPPJWT';

    public const ACCJWT = 'ACCJWT';

    public const REFJWT = 'REFJWT';

    public const SERJWT = 'SERJWT';

    public const SECJWT = 'SECJWT';

    public const ACTJWT = 'ACTJWT';

    /**
     * @method validates or decrypt tokens.
     * @param string keypath or keycontent
     * @param bool is file ? is file - true if 
     * it is rsa content
     * @throws EoJwtException
     */
    public function validate(string $_key_path_or_content, bool $_is_file = false): void;
    /**
     * @method populates full token params
     * @return Jwt|AccJwt|RefreshJwt|ServJwt
     * @throws EoJwtException
     */
    public function payload(): Jwt|AccJwt|RefreshJwt|SessionJwt|SecureAccountJwt|ServJwt|ActiveToken;
    /**
     * @method server
     * @return ServerAccount
     */
    public function server(): ServerAccount;
    /**
     * @method user
     * @return UserAccount
     */
    public function user(): UserAccount;
    /**
     * @return string session file
     */
    public function getSessionFile(): string;
    /**
     * @method populates token hash
     * @return string full token body
     */
    public function token(): string;

}