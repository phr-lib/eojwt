<?php

namespace Phr\Eojwt\EoJwtTools;

use Phr\Certificator\Encryption;
use Phr\Certificator\Encry\HashAlgo as ALG;


/**
 * @abstract encoders
 * 
 * Base for Accounts payload encodeing and
 * decodeing
 * 
 */
abstract class EoJwtEncoder
{
    public function json(): string
    {
        return json_encode($this);
    }
    public function jsonDecode($_data): array|object
    {
        return json_decode($_data);
    }
    public function encode(): string
    {
        return base64_encode($this->json());
    }
    public function decode(string $_encoded): string
    {
        return base64_decode($_encoded);
    }
    public function encodeHex(): string
    {
        return bin2hex($this->encode());
    }
    public function decodeHex(string $_encoded): string
    {   
        return $this->decode(hex2bin($_encoded));
    }
    public function hash(ALG $ALG): string 
    {
        return Encryption::hashIt($this->json(), $ALG);
    }
    public function object(string $_encoded): object
    {
        return $this->jsonDecode($this->decode($_encoded));
    }
    public function hex(string $_content): string
    {
        return bin2hex($_content);
    }
    public function dehex(string $_content): string
    {
        return hex2bin($_content);
    }
}