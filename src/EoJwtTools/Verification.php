<?php

namespace Phr\Eojwt\EoJwtTools;

use Phr\Eojwt\EoJwtException;
use Phr\Eojwt\EoJwtBase\Errors as ERR;

class Verification
{   
    private const BEGIN_PUBLIC = '-----BEGIN PUBLIC KEY-----';

    private const END_PUBLIC = '-----END PUBLIC KEY-----';

    private const BEGIN_PRIVATE = '-----BEGIN PRIVATE KEY-----';

    private const END_PRIVATE = '-----END PRIVATE KEY-----';


    public static function publicRsa(string $_publc_key): true
    {
        if(!preg_match('/'.self::BEGIN_PUBLIC.'/', $_publc_key)) throw new EoJwtException(ERR::E5393203);
        if(!preg_match('/'.self::END_PUBLIC.'/', $_publc_key)) throw new EoJwtException(ERR::E5393203);
        return true;
    }
    public static function privateRsa(string $_private_key): true
    {
        if(!preg_match('/'.self::BEGIN_PRIVATE.'/', $_private_key)) throw new EoJwtException(ERR::E5393204);
        if(!preg_match('/'.self::END_PRIVATE.'/', $_private_key)) throw new EoJwtException(ERR::E5393204);
        return true;
    }
}