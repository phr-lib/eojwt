<?php

namespace Phr\Eojwt\EoJwtBase;

enum Errors 
{
    case E5393000;
    case E5393001;

    case E5393011;
    case E5393012;
    case E5393013;

    case E5393201;
    case E5393202;
    case E5393203;
    case E5393204;



    case E5393501;
    case E5393502;
    case E5393505;







    public function code(): int 
    {
        return match ($this) {
            self::E5393000 => 5393000,
            self::E5393001 => 5393001,

            self::E5393011 => 5393011,
            self::E5393012 => 5393012,
            self::E5393001 => 5393013,

            self::E5393201 => 5393201,
            self::E5393202 => 5393201,
            self::E5393203 => 5393203,
            self::E5393204 => 5393204,



            self::E5393501 => 5393501,
            self::E5393502 => 5393502,
            self::E5393505 => 5393505




            
        };
    }
    public function message(): string 
    {
        return match ($this) {
            self::E5393000 =>  'Jwt error',
            self::E5393001 =>  'Malformed token parts',

            self::E5393011 =>  'Missing token header',
            self::E5393012 =>  'Missing token payload',
            self::E5393013 =>  'Missing token signature',

            self::E5393201 =>  'Missing rsa key path',
            self::E5393202 =>  'Can not read rsa key content',
            self::E5393203 =>  'not valid public key',
            self::E5393204 =>  'not valid private key',


            self::E5393501 =>  'Jwt is expired',
            self::E5393502 =>  'Can not verify private signature',

            self::E5393505 =>  'Signature is not valid'






            
        };
    }
}