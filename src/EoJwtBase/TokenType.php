<?php

namespace Phr\Eojwt\EoJwtBase;

use Phr\Eojwt\IEoJwt;
use Phr\Certificator\Encry\HashAlgo as ALG;


enum TokenType
{
    case PHRJWT;
    case PPPJWT;
    case ACCJWT;

    case REFJWT;
    case SERJWT;

    case SECJWT;
    case ACTJWT;






    public function type(): string
    {
        return match ($this) {
            self::PHRJWT => IEoJwt::PHRJWT,
            self::PPPJWT => IEoJwt::PPPJWT,
            self::ACCJWT => IEoJwt::ACCJWT,
            self::REFJWT => IEoJwt::REFJWT,
            self::SERJWT => IEoJwt::SERJWT,
            self::SECJWT => IEoJwt::SECJWT,
            self::ACTJWT => IEoJwt::ACTJWT,


        };
    }
    public function algoContent(): ALG
    {
        return match ($this) {
            self::PHRJWT => ALG::SHA256,
            self::PPPJWT => ALG::SHA256,
            self::ACCJWT => ALG::SHA256,
            self::REFJWT => ALG::SHA256,
            self::SERJWT => ALG::SERJWT,
            self::SECJWT => ALG::SHA256,
            self::ACTJWT => ALG::SHA256,

        };
    }
    public function algoSignature(): ALG
    {
        return match ($this) {
            self::PHRJWT => ALG::MD5,
            self::PPPJWT => ALG::MD5,
            self::ACCJWT => ALG::MD5,
            self::REFJWT => ALG::MD5,
            self::SERJWT => ALG::MD5,
            self::SECJWT => ALG::MD5,
            self::ACTJWT => ALG::MD5,

        };
    }
    
}