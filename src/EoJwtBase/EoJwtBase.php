<?php

namespace Phr\Eojwt\EoJwtBase;

use Phr\Eojwt\JwtModel\Headers\JwtHeader;
use Phr\Eojwt\JwtModel\Signatures\JwtSignature;


use Phr\Eojwt\JwtModel\Signatures\EoJwtSignature;
use Phr\Eojwt\JwtModel\Signatures\EoSecureSignature;
use Phr\Eojwt\JwtModel\Signatures\EncrySignature;
use Phr\Eojwt\JwtModel\Signatures\SimpleTimeSignature;

use Phr\Eojwt\EoJwtBase\TokenType;
use Phr\Certificator\Encry\HashAlgo as ALG;
use Phr\Eojwt\EoJwtException;
use Phr\Eojwt\EoJwtBase\Errors as ERR;

/**
 * @abstract 
 * @see EoJwt
 */
abstract class EoJwtBase 
{   
    
    protected JwtHeader $header;

    protected static TokenType $tokenType;

    protected string $issuer;

    protected \DateTime $created;

    protected \DateTime $expire;

    protected JwtSignature|EoJwtSignature|EoSecureSignature|EncrySignature|SimpleTimeSignature $signature;

    protected static string $tokenId;

    protected static string $signatureKey;

    protected static int $expiretime;

    protected static string|null $contentEncryptor = null;

    protected static string $sessionIv = '1234567891011121';

    protected function timeHash(): string
    {   
        return md5(self::$expiretime.self::$tokenId);
    }
    public function __construct()
    {
        $this->header = new JwtHeader;
    }

    protected static function setRsaContent(string|null $_full_key_path): void 
    {
        if(!file_exists($_full_key_path)) throw new EoJwtException(ERR::E5393201);
        $content = file_get_contents($_full_key_path);
        if(!$content) throw new EoJwtException(ERR::E5393202);
        self::$signatureKey = $content;
    }
    public function setSessionId(string $_session_id): void
    {
        $this->header->sessionId = $_session_id;
    }
    public function getSessionId(): string
    {
        return $this->header->sessionId;
    }
    
}
