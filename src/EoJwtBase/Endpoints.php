<?php

namespace Phr\Eojwt\EoJwtBase;

use Phr\Eojwt\IEndpoints;

class Endpoints implements IEndpoints
{   
    public string $info;

    public string $user;

    public string $token;

    public string $admin;

    private const SP = '/';

    public function __construct(string $_isshuer)
    {
        $this->info = $_isshuer.self::SP.self::INFO;
        $this->user = $_isshuer.self::SP.self::USER;
        $this->token = $_isshuer.self::SP.self::TOKEN;
        $this->admin = $_isshuer.self::SP.self::ADMIN;


    }

}