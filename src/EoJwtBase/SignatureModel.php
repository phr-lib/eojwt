<?php

namespace Phr\Eojwt\EoJwtBase;

use Phr\Eojwt\EoJwtTools\EoJwtEncoder;
use Phr\Eojwt\EoJwtBase\TokenType;
use Phr\Eojwt\EoJwtException;
use Phr\Eojwt\EoJwtBase\Errors as ERR;
use Phr\Certificator\Encryption;
use Phr\Eojwt\EoJwtTools\Verification;



abstract class SignatureModel extends EoJwtEncoder
{   
    abstract public function content(string $_encoded_signature): void;
    abstract public function validate( string $_key ): true;

    protected static TokenType $TYPE;

    public string $signature;

    protected string|null $timehash;

    protected string|null $contentHash;

    public function params(TokenType $TYPE, string|null $_timehash = null)
    {
        self::$TYPE = $TYPE;
        $this->timehash = $_timehash;
    }
    public function finalSignature(string $_signature): void
    {   
        $this->signature = $_signature;
    }
    protected function publicDecrypt( string $_key, string $_content ): string
    {     
        Verification::publicRsa($_key);  
        $result = Encryption::sslPublicDecrypt($_key, $_content);
        return $result ? $result: throw new EoJwtException(ERR::E5393502);
    }
    protected function privateDecrypt( string $_key, string $_content ): string
    {     
        Verification::privateRsa($_key);  
        $result = Encryption::sslPrivateDecrypt($_key, $_content);
        return $result ? $result: throw new EoJwtException(ERR::E5393502);
    }
    protected function sslDecrypt( string $_key, string $_encoded_content )
    {
        $encrypt = new Encryption($_key);
        return $encrypt->fernetDecrypt($_encoded_content);
    }
    public function setContentHash(string $_content_hash): void
    {
        $this->contentHash = $_content_hash;
    }
    public static function simpleTimeSig(string $_time_hash, string $_signature_key)
    {   
        return Encryption::hashIt($_time_hash.$_signature_key);
    }
    
}