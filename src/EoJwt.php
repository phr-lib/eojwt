<?php

namespace Phr\Eojwt;

use Phr\Eojwt\EoJwtBase\EoJwtBase;
use Phr\Certificator\CertificatorBase\Macro\Gradients as GR;
use Phr\Eojwt\EoJwtException;
use Phr\Eojwt\EoJwtBase\Errors as ERR;
use Phr\Eojwt\JwtModel\Headers\JwtHeader;
use Phr\Eojwt\Tokens\Jwt;
use Phr\Eojwt\Tokens\AccJwt;
use Phr\Eojwt\Tokens\RefreshJwt;
use Phr\Eojwt\Tokens\ServJwt;
use Phr\Eojwt\Tokens\SessionJwt;
use Phr\Eojwt\Tokens\SecureAccountJwt;
use Phr\Eojwt\Tokens\ActiveJwt;

use Phr\Eojwt\EoJwtBase\TokenType;
use Phr\Eojwt\JwtModel\Signatures\JwtSignature;
use Phr\Eojwt\JwtModel\Signatures\EoSecureSignature;
use Phr\Eojwt\JwtModel\Signatures\EoJwtSignature;
use Phr\Eojwt\JwtModel\Signatures\EncrySignature;
use Phr\Eojwt\JwtModel\Signatures\SimpleTimeSignature;

use Phr\Eojwt\Accounts\UserAccount;
use Phr\Eojwt\Accounts\ServerAccount;
use Phr\Eojwt\Accounts\ActiveToken;


/**
 * @final
 * 
 * 
 * 
 * @see interface IEoJwt for public details
 * 
 * 
 * 
 */
final class EoJwt extends EoJwtBase implements IEoJwt
{   
    private string $tempHeader;

    private string $tempPayload;

    private string $tempSignature;

    private Jwt|AccJwt|RefreshJwt|SessionJwt|SecureAccountJwt|ServJwt|ActiveToken $payload;

    public function __construct(string $_token)
    {   
        if(preg_match('/'.GR::COMA.'/', $_token))
        {   
            parent::__construct();

            $tokenParts = explode(GR::COMA, $_token);
            if(count($tokenParts) !== 3) throw new EoJwtException(ERR::E5393001);
            if(!isset($tokenParts[0])) throw new EoJwtException(ERR::E5393011);
            if(!isset($tokenParts[1])) throw new EoJwtException(ERR::E5393012);
            if(!isset($tokenParts[2])) throw new EoJwtException(ERR::E5393001);
            /**
             * Set and precheck token header.
             */
            $this->header->content($tokenParts[0]);
            
            self::$tokenType =  $this->header->getType();
            $this->validateHeader();
            /**
             * Set and define singnature variant.
             */
            $this->tempSignature = $tokenParts[2];
            $this->signature = $this->newSignature(); 
            $this->signature->params(self::$tokenType, $this->timehash);
            $this->signature->content($tokenParts[2]);
            /**
             * Prepair payload validation.
             */
            $this->payloadValidation($tokenParts[1]);
        }
    }
    public function decryptContent(string $_key): void
    {
        $this->payload->decryptPayload($_key, self::$sessionIv);
    }
    public function validate(string $_key_path_or_content, bool $_is_file = true): void
    {   
        if($_is_file == true) parent::setRsaContent($_key_path_or_content);
        else self::$signatureKey = $_key_path_or_content;
        
        $this->signature->validate(self::$signatureKey);
        
        if($this->signature instanceof EoSecureSignature)
        {   
            $this->payload->content($this->signature->getContentEncoder());
        }
    }
    public function payload(): Jwt|AccJwt|RefreshJwt|SessionJwt|ServJwt|ActiveToken
    {
        return $this->payload;
    }
    public function server(): ServerAccount
    {
        if(($this->payload instanceof ServJwt))
        {
            return $this->payload->account;
        }
    }
    public function user(): UserAccount
    {           
        if(($this->payload instanceof SecureAccountJwt))
        {
            return $this->payload->account;
        }
    }
    public function getSessionFile(): string
    {
        return $this->signature->signature;
    }
    public function token(): string
    {
        return $this->payload->getContentHash();
    }
    /**
     * @access private
     * @method fast validate datetime
     * @throws EoJwtException
     */
    private function validateHeader(): void
    {   
        if(!$this->header->tokenId) throw new EoJwtException(ERR::E5393001);
        /**
         * Token id contains id and
         * expire timestamp. System can refuse
         * token base on precalculations.
         * 
         */
        $headerParts = explode(GR::COMA, $this->header->tokenId);
        if(!isset($headerParts[0])) throw new EoJwtException(ERR::E5393001);
        if(!isset($headerParts[1])) throw new EoJwtException(ERR::E5393001);
        self::$tokenId = $headerParts[0];
        $this->expire = new \DateTime($this->header->expire);
        /**
         * Save time hash for further verification.
         */
        $this->timehash = md5($headerParts[1].$headerParts[0]);
        /**
         * Time to live precheck. 
         */
        if($this->expire < new \DateTime('now')) throw new EoJwtException(ERR::E5393501);

    }
    private function setJwtPayload(): void
    {
        switch ( parent::$tokenType ) 
        {
            case TokenType::PHRJWT: $token =  new Jwt; break;
            case TokenType::PPPJWT: $token =  new Jwt; break;
            case TokenType::ACCJWT: $token =  new AccJwt; break;
            case TokenType::REFJWT: $token =  new RefreshJwt; break;
            case TokenType::SERJWT: $token =  new ServJwt; break;
            case TokenType::SECJWT: $token =  new SecureAccountJwt; break;
            case TokenType::ACTJWT: $token =  new ActiveToken; break;
        }
        $this->payload = $token;
    }
    private static function newSignature()
    {   
        return match (self::$tokenType) {
            TokenType::PHRJWT => new JwtSignature,
            TokenType::PPPJWT => new JwtSignature,
            TokenType::ACCJWT => new EoSecureSignature,
            TokenType::REFJWT => new EncrySignature,
            TokenType::SERJWT => new EoSecureSignature,
            TokenType::SECJWT => new JwtSignature,
            TokenType::ACTJWT => new SimpleTimeSignature
        };
    }
    private function payloadValidation(string $_encrypted_payload)
    {   
        $this->setJwtPayload();

        if($this->signature instanceof JwtSignature)
        {   
            $this->payload->content($_encrypted_payload, self::$tokenType);
            if($this->payload instanceof SecureAccountJwt){

            }else
            $this->signature->setContentHash($this->payload->getContentHash());
        }
        elseif($this->signature instanceof EoSecureSignature)
        {   
            $this->payload->setEcodedPayload($_encrypted_payload);        
            $this->payload->setTimeHash($this->timehash);   
        }
        elseif($this->signature instanceof EncrySignature)
        {   
            $this->payload->content($_encrypted_payload, self::$tokenType);
        }
        elseif($this->signature instanceof SimpleTimeSignature)
        {   
            $this->signature->setTimeHash($this->timehash);
            $this->payload->content($this->getSessionId(), $_encrypted_payload);
        }
        
    }
}