<?php

namespace Phr\Eojwt\Accounts;

use Phr\Eojwt\Accounts;

class ServerAccount
{
    public string $serverId;

    public SessionAccount $sessionAccount;

    public string $clientIp;

    public function __construct(
        string $_server_id,
        SessionAccount $_session_account,
        string $_client_ip
    ){
        $this->serverId = $_server_id;
        $this->sessionAccount = $_session_account;
        $this->clientIp = $_client_ip;
    }
}