<?php

namespace Phr\Eojwt\Accounts;

class SessionFp
{   
    public string $fp1;
    public string $fp2;
    public string $fp3;
    public string $fp4;
    public string $fp5;

    public function __construct(array $_finger_prints)
    {
        $this->fp1 = $_finger_prints[0];
        $this->fp2 = $_finger_prints[1];
        $this->fp3 = $_finger_prints[2];
        $this->fp4 = $_finger_prints[3];
        $this->fp5 = $_finger_prints[4];
    }
}