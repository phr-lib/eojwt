<?php

namespace Phr\Eojwt\Accounts;

class ActiveSession
{
    public string $accountId;

    public SessionFp $sessionFp;

    public function __construct(
        string $_account_id,
        SessionFp $_session_fp
    ){
        $this->accountId = $_account_id;
        $this->sessionFp = $_session_fp;
    }
}