<?php

namespace Phr\Eojwt\Accounts;


class SessionAccount
{
    public string $userId;

    public string $sessionId;

    public int $sessionTs;

    public int $expire;

    public string $enryptor;

    public string $sessionIv;

    public SessionFp $sessionFingerprints;

    public function __construct(
        string $_user_id,
        string $_session_id,
        int $_session_ts,
        int $_expire,
        string $_enryptor,
        string $_session_iv,
        SessionFp $_session_fingerprints
    ){
        $this->userId = $_user_id;
        $this->sessionId = $_session_id;
        $this->sessionTs = $_session_ts;
        $this->expire = $_expire;
        $this->enryptor = $_enryptor;
        $this->sessionIv = $_session_iv;
        $this->sessionFingerprints = $_session_fingerprints;
    }
}