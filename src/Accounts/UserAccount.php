<?php

namespace Phr\Eojwt\Accounts;


class UserAccount
{   
    public string $username;

    public string $realmId;

    public string $accountId;

    public string $sessionId;

    public string $encryptor;

    public string $sessionIv;

    public UserRols $userRols;


    public function __construct(
        string $_account_id,
        string $_realm_id,
        string $_username,
        string $_session_id,
        string $_encryptor,
        string $_sessionIv,
        UserRols $_user_rols = null
    ){
        $this->accountId = $_account_id;
        $this->realmId = $_realm_id;
        $this->username = $_username;
        $this->sessionId = $_session_id;
        $this->encryptor = $_encryptor;
        $this->sessionIv = $_sessionIv;
        $this->userRols = $_user_rols;
    }
    public function json()
    {
        return json_encode($this);
    }
}