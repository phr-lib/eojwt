<?php

namespace Phr\Eojwt\Accounts;

use Phr\Eojwt\Tokens\ActiveJwtBase;

class ActiveToken
{
    public string $sessionId;

    public string $token;


    public function content(
        string $_session_id,
        string $_token_content
    ){
        $this->sessionId = $_session_id;
        $this->token = $_token_content;
    }
}