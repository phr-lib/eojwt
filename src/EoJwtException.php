<?php

namespace Phr\Eojwt;

use Phr\Eojwt\EoJwtBase\Errors as ERR;

class EoJwtException extends \Exception 
{
    public function __construct(ERR $ERROR, $_message = "")
    {
        $this->code = $ERROR->code();
        $this->message = $ERROR->message().' '.$_message;
    }
}